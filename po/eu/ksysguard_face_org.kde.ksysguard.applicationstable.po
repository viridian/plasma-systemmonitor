# Translation for ksysguard_face_org.kde.ksysguard.applicationstable.po to Euskara/Basque (eu).
# Copyright (C) 2020-2023 This file is copyright:
# This file is distributed under the same license as the plasma-systemmonitor package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-systemmonitor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-01 01:41+0000\n"
"PO-Revision-Date: 2023-01-12 21:29+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.1\n"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:48
#, kde-format
msgctxt "@title:window"
msgid "Details"
msgstr "Xehetasunak"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:72
#, kde-format
msgctxt "@title:group"
msgid "CPU"
msgstr "PUZ"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:124
#, kde-format
msgctxt "@title:group"
msgid "Memory"
msgstr "Memoria"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:150
#, kde-format
msgctxt "@title:group"
msgid "Network"
msgstr "Sarea"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:175
#, kde-format
msgctxt "@title:group"
msgid "Disk"
msgstr "Diskoa"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:201
#, kde-format
msgctxt "@title:group"
msgid "Processes: %1"
msgstr "Prozesuak: %1"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:262
#, kde-format
msgctxt "@info:placeholder"
msgid "Select an application to see its details"
msgstr "Hautatu aplikazio bat haren xehetasunak ikusteko"

#: src/faces/applicationstable/contents/ui/ApplicationsTableView.qml:28
#, kde-format
msgctxt "Warning message shown on runtime error"
msgid "Applications view is unsupported on your system"
msgstr "Zure sistemak ez du onartzen Aplikazioen ikuspegia"

#: src/faces/applicationstable/contents/ui/Config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Show details panel"
msgstr "Erakutsi xehetasun-panela"

#: src/faces/applicationstable/contents/ui/Config.qml:27
#, kde-format
msgctxt "@option:check"
msgid "Confirm when quitting applications"
msgstr "Berretsi aplikazioetatik irtetea"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:28
#, kde-format
msgctxt "@action"
msgid "Search"
msgstr "Bilatu"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:39
#, kde-format
msgctxt "@action"
msgid "Quit Application"
msgstr "Irten aplikaziotik"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:47
#, kde-format
msgctxt "@action"
msgid "Show Details Sidebar"
msgstr "Erakutsi xehetasunen alboko-barra"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:55
#, kde-format
msgctxt "@action"
msgid "Configure columns…"
msgstr "Konfiguratu zutabeak..."

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:185
#, kde-format
msgctxt "@action:inmenu"
msgid "Set priority…"
msgstr "Ezarri lehentasuna..."

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:188
#, kde-format
msgctxt "@action:inmenu"
msgid "Send Signal"
msgstr "Bidali seinalea"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:191
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Suspend (STOP)"
msgstr "Egonean utzi (STOP)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:195
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Continue (CONT)"
msgstr "Jarraitu (CONT)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:199
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Hangup (HUP)"
msgstr "Eseki (HUP)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:203
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Interrupt (INT)"
msgstr "Eten (INT)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:207
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Terminate (TERM)"
msgstr "Bukatu (TERM)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:211
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Kill (KILL)"
msgstr "Akabatu (KILL)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:215
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "User 1 (USR1)"
msgstr "1 erabiltzailea (USR1)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:219
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "User 2 (USR2)"
msgstr "2 erabiltzailea (USR2)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:226
#, kde-format
msgctxt "@action:inmenu"
msgid "Quit Application"
msgid_plural "Quit %1 Applications"
msgstr[0] "Irten aplikaziotik"
msgstr[1] "Irten %1 aplikaziotatik"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:244
#, kde-format
msgctxt "@title:window"
msgid "Quit Application"
msgid_plural "Quit %1 Applications"
msgstr[0] "Irten aplikaziotik"
msgstr[1] "Irten %1 aplikaziotatik"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:245
#, kde-format
msgctxt "@action:button"
msgid "Quit Application"
msgid_plural "Quit Applications"
msgstr[0] "Irten aplikaziotik"
msgstr[1] "Irten aplikazioetatik"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:247
#, kde-format
msgid ""
"Are you sure you want to quit this application?\n"
"Any unsaved work may be lost."
msgid_plural ""
"Are you sure you want to quit these %1 applications?\n"
"Any unsaved work may be lost."
msgstr[0] ""
"Ziur zaude aplikazio honetatik irten nahi duzula?\n"
"Gorde gabeko lana gal daiteke."
msgstr[1] ""
"Ziur zaude %1 aplikazio hauetatik irten nahi duzula?\n"
"Gorde gabeko lana gal daiteke."

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:261
#, kde-format
msgctxt "@item:intable"
msgid "%1 Process"
msgid_plural "%1 Processes"
msgstr[0] "Prozesu %1"
msgstr[1] "%1 prozesu"

#~ msgctxt "@action:button"
#~ msgid "Quit"
#~ msgstr "Irten"
